#!/usr/bin/python
# -*- coding: utf_8 -*-
# coding: UTF-8
import web,os,configparser,re,socket,platform,struct,subprocess,sys 
from web import form
# if platform.system() == 'Linux':
render = web.template.render(sys.path[0] + '/templates/')

urls = (
    '/', 'index'
)
class confile(object):
        
    def __init__(self):
#         if platform.system() == 'Linux':
        self.file_path = os.path.dirname(sys.path[0]) + '/connect_info.ini'
        self.conpar = self.read_config_file()
        self.localip = self.get_ip_address('eth0')
        self.gateway = self.get_gateway()
        self.bcast = self.get_bcast_mask()[1]
        self.mask = self.get_bcast_mask()[2]
        self.password = 'xindong'
        self.ipbootproto = self.get_ipbootproto()
        
    def get_gateway(self):
        try:
            pattern = re.compile(r'((?:\d{1,3}\.){3})\d{1,3}')
            match = pattern.match(self.localip)
            return match.groups()[0]+'1'
        except:
            return None
    
    def get_ipbootproto(self):
        try:
            if platform.system() == 'Linux':
                interfaces = '/etc/network/interfaces'
            else:
                interfaces = 'D:/workspace/yjscript/test.txt'
            with open(interfaces,'r') as r_file:
                keywords = r'.iface eth0 inet (\w{4,6})'
                patten = re.compile(keywords)
                ret = 'dhcp'
                for finditer in re.finditer(patten,r_file.read()):
                    ret =  finditer.groups()[0]
            return ret
        except:
            return None
    
    def read_config_file(self):
        try:
            conf = configparser.ConfigParser()
            conf.read(self.file_path)
#             print conf.items(conf.sections()[0])
            return conf.items(conf.sections()[0])
        except:
            return None
    
    def get_ip_address(self,ifname):
        try:
            if(platform.system() == 'Linux'):
                import fcntl
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
                return socket.inet_ntoa(fcntl.ioctl(s.fileno(),0x8915,struct.pack('256s', ifname[:15]))[20:24]) 
            elif(platform.system() == 'Windows'):
                localIP = socket.gethostbyname(socket.gethostname())
    #             print "local ip:%s "%localIP
    #             ipList = socket.gethostbyname_ex(socket.gethostname())
    #             print ipList
                return localIP
        except:
            return None
    def get_bcast_mask(self):
        try:
            ipstr = r'((?:[0-9]{1,3}\.){3}[0-9]{1,3})';
            if platform.system() == "Darwin" or platform.system() == "Linux":
                ipconfig_process = subprocess.Popen("ifconfig", stdout=subprocess.PIPE)
                output = ipconfig_process.stdout.read()
                ip_pattern = re.compile('(inet %s)' % ipstr)
                if platform.system() == "Linux":
                    ip_pattern = re.compile('(inet addr:%s)' % ipstr)
                pattern = re.compile(ipstr)
                bcast_mask = []
                flag = 0
                for ipaddr in re.finditer(pattern, str(output)):
                    ip = pattern.search(ipaddr.group())
                    if ip.group() == self.localip:flag = 1
                    if flag:
                        bcast_mask.append(ip.group()) 
                    if len(bcast_mask) == 3:break 
                return bcast_mask
            elif platform.system() == "Windows":
                
                ipconfig_process = subprocess.Popen("ipconfig", stdout=subprocess.PIPE)
                output = ipconfig_process.stdout.read()
                ip_pattern = re.compile(ipstr)
                pattern = re.compile(ipstr)
                bcast_mask = []
                flag = 0
                for ipaddr in re.finditer(ip_pattern, str(output)):
                    ip = pattern.search(ipaddr.group())
                    if ip.group() == self.localip:flag = 1
                    if flag:
                        bcast_mask.append(ip.group()) 
                    if len(bcast_mask) == 3:break
                jiaohuan = bcast_mask[1]  
                bcast_mask[1] = bcast_mask[2] 
                bcast_mask[2] = jiaohuan                  
                return bcast_mask   
        except:
            return [None,None,None]
                   
    def set_ipconf(self,acquisition,address='192.168.1.88',netmask='255.255.255.0',gateway='192.168.1.1'):
        if platform.system() == 'Linux':
            interfaces = '/etc/network/interfaces'
        else:
            interfaces = 'D:/workspace/yjscript/test.txt'  
        pattern = re.compile(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')
        match_1 = pattern.match(address)
        match_2 = pattern.match(netmask)
        match_3 = pattern.match(gateway)
        if(acquisition != 'dhcp' and acquisition != 'static') or (match_1 == None) or (match_2 == None) or (match_3 == None):
            print 'wrong data'
            return 'wrong data'
        if (acquisition=='dhcp'):
            ip = "        iface eth0 inet "+ acquisition
        else:
            ip = "        iface eth0 inet "+ acquisition\
            +'\n        address ' + address\
            +'\n        netmask ' + netmask\
            +'\n        gateway ' + gateway
        w_file = open(interfaces,'w')
        try:
            str1 = '''
            # interfaces(5) file used by ifup(8) and ifdown(8)
            # Include files from /etc/network/interfaces.d:
            source-directory /etc/network/interfaces.d
            
            auto lo
            iface lo inet loopback
            
            auto eth0
            allow-hotplug eth0
            '''
            #         iface eth0 inet static
            #         address 192.168.1.88
            #         netmask 255.255.255.0
            #         gateway 192.168.1.1 
                    
            str2='''
            allow-hotplug wlan0
            auto wlan0
            iface wlan0 inet static
                    address 192.168.8.1
                    netmask 255.255.255.0
            '''   
             
            wstring =str1 + ip + str2 
            w_file.write(wstring)
        #     print w_file.readlines()
        #     print w_file.read()
        finally:
            w_file.close()
        return 'set ip successful'
    
    def ip_re(self,ip,port = 9999):
        pattern_1 = re.compile(r'^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$')
        match_1 = pattern_1.match(ip)
        if(match_1 == None ):
#             print "wrong data:   %s" %ip
            return "wrong data: "+ip
        if not str(port).isdigit() or int(port) > 65535 or int(port) <= 0:
#             print "wrong data:   %s" %port
            return "wrong data: "+port 
        return 0    
          
    def set_connect_info(self,\
                         hostname = '192.168.1.160',\
                         port = '1883',\
                         username = 'yj',\
                         password = 'yj12345',\
                         topic = 'things',\
                         runstate = '1',\
                         interfaces = 'test.txt',\
                         client_ip_acquisition = 'static',\
                         client_ip = '192.168.1.188',\
                         client_netmask = '255.255.255.0',\
                         client_gateway = '192.168.1.1',\
                         publish_interval = '30'):
        if (not os.path.exists(self.file_path)): 
            f=open(self.file_path,'w') 
            f.write('''[info]
                    hostname = 192.168.1.180
                    port = 1883
                    client_id = TG-200-20160003
                    keepalive = 60
                    will = 1
                    username = yj
                    password = yj12345
                    subscribe_qos = 2
                    topic = things
                    pub_qos = 1
                    pub_retain = 0
                    runstate = 1
                    publish_interval = 4'''
                    )   
            f.close()  
        re_ip = self.ip_re(hostname,port)
        if re_ip != 0:
            return re_ip
        re_ip = self.ip_re(client_ip)
        if re_ip != 0:
            return re_ip
        re_ip = self.ip_re(client_netmask)
        if re_ip != 0:
            return re_ip
        re_ip = self.ip_re(client_gateway)
        if re_ip != 0:
            return re_ip
        conf = configparser.ConfigParser()
        conf.read(self.file_path)
        conf.set('Mqtt', 'hostname', hostname)
        conf.set('Mqtt', 'port',port)
        conf.set('Mqtt', 'username',username)
        conf.set('Mqtt', 'password',password)
        conf.set('Mqtt', 'topic',topic)
        conf.set('Mqtt', 'publish_interval',publish_interval)
#         conf.set('Mqtt', '11.runstate',runstate)
    #     conf.set('Mqtt', '2.client_id',client_id)
        conf.write(open(self.file_path, "w"))
        ret = self.set_ipconf(client_ip_acquisition,client_ip,client_netmask,client_gateway) 
        if(ret != 'set ip successful'):
            print 'set connect info failed'
            return 'set connect info failed'
        print "set connect info successful"   
        return 0
    
    def get_connect_info(self):
        p = self.read_config_file(self.file_path)
        localip = self.get_ip_address('eth0')
        rp = {'_server_ip':p[0][0],'_server_port':p[0][1],'_server_username':p[0][5],'_server_password':p[0][6],'_server_name':p[0][8],\
            '_client_id':p[0][2],'_client_ip':localip,'_publish_interval':p[0][12]}  
        return rp  
        
app = web.application(urls, globals())
def initrender():    
    myconfile = confile()
    textboxlist = []
    for x in myconfile.conpar:
        textboxlist.append(form.Textbox(x[0],value = x[1]))
    if myconfile.ipbootproto == 'dhcp':
        ipboot = ['dhcp','static']
    else:
        ipboot = ['static','dhcp',] 
#     textboxlist[5].set_value('xxxxx')      
    mylogin = form.Form(
                        textboxlist[0],textboxlist[1],textboxlist[5],
                        textboxlist[6],textboxlist[8],textboxlist[12],
                        form.Textbox('Local_IP',value = myconfile.localip),
                        form.Textbox('Gateway',value = myconfile.gateway),
                        form.Textbox('Mask',value = myconfile.mask),
                        form.Dropdown('IPBOOTPROTO', ipboot),
                        form.Textbox('Modify_permissions_password'),
                        validators = [form.Validator("", lambda i: i.Modify_permissions_password == myconfile.password)]
    #     form.Password('password'),
    #     form.Password('password_again'),  
    # 
    #     form.Button('Login'),
    #     form.Checkbox('YES'),
    #     form.Checkbox('NO'),
    #     form.Textarea('moe'),
    #     form.Dropdown('IPBOOTPROTO', ['dhcp', 'static']),
    #     form.Radio('time',['2012-01-01','20120101']),
    #     validators = [form.Validator("Passwords didn't match.", lambda i: i.password == i.password_again)]
    )
    return mylogin,myconfile
    
class index:
    def GET(self):
        f=initrender()
#         for i in f.inputs:
#             print i
#         print f.inputs[0].value
        return render.formtest(f[0],f[1].conpar)
    def POST(self):
        f,myconfile=initrender()
        
#         print f.render()

        if not f.validates():            
            return render.formtest(f,myconfile.conpar)
        else:
            ret = myconfile.set_connect_info(hostname = f.inputs[0].value, 
                                        port = f.inputs[1].value, 
                                        username = f.inputs[2].value, 
                                        password = f.inputs[3].value, 
                                        topic = f.inputs[4].value,
                                        publish_interval = f.inputs[5].value,
                                        client_ip_acquisition = f.inputs[9].value, 
                                        client_ip = f.inputs[6].value, 
                                        client_netmask = f.inputs[8].value, 
                                        client_gateway = f.inputs[7].value 
                                        )
            if not ret: 
                os.popen('reboot')     
#                 return '<p style="color:blue;font-size:30px;">Configuration parameter successful ! System will restart !</p>'
            else:
                return render.formtest(f,myconfile.conpar,ret)


if __name__ == "__main__":
#    web.internalerror = web.debugerror
    app.run()

#!/usr/local/bin/python2.7
# -*- coding: utf_8 -*-
# encoding: UTF-8
import ConfigParser
import re, os, sys, time, xlrd
import yj_global
import socket 
import struct 
import platform
import gzip, binascii 
from cStringIO import StringIO  
# from pyatspi.interface import interface
# from symbol import except_clause

# import fcntl 
def get_ip_address(ifname='enp5s0'): 
#     import requests
#     getstr = requests.get("http://city.ip138.com/ip2city.asp").content.decode('cp936').encode('utf-8')
#     ip = re.search(r'(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})',getstr).group()
#     print ip
    if(platform.system() == 'Linux'):
        import fcntl
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s', ifname[:15]))[20:24]) 
# get_ip_address('lo')
# get_ip_address('eth0')
    elif(platform.system() == 'Windows'):
        localIP = socket.gethostbyname(socket.gethostname())
        print "local ip:%s " % localIP
        ipList = socket.gethostbyname_ex(socket.gethostname())
        print ipList
        return localIP


def set_ipconf(file_path, acquisition, address='192.168.1.88', netmask='255.255.255.0', gateway='192.168.1.1'):
    # w_file = open('test.txt','r')
    pattern = re.compile(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')
    match_1 = pattern.match(address)
    match_2 = pattern.match(netmask)
    match_3 = pattern.match(gateway)
    if(acquisition != 'dhcp' and acquisition != 'static') or (match_1 == None) or (match_2 == None) or (match_3 == None):
        print 'wrong data'
        return 'wrong data'
    if (acquisition == 'dhcp'):
        ip = "        iface eth0 inet " + acquisition
    else:
        ip = "        iface eth0 inet " + acquisition\
        + '\n        address ' + address\
        + '\n        netmask ' + netmask\
        + '\n        gateway ' + gateway
    w_file = open(file_path, 'w')
    try:
        str1 = '''
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

auto lo
iface lo inet loopback

auto eth0
allow-hotplug eth0
'''
#         iface eth0 inet static
#         address 192.168.1.88
#         netmask 255.255.255.0
#         gateway 192.168.1.1 
        
        str2 = '''
allow-hotplug wlan0
auto wlan0
iface wlan0 inet static
        address 192.168.8.1
        netmask 255.255.255.0
'''   
         
        wstring = str1 + ip + str2 
        w_file.write(wstring)
    #     print w_file.readlines()
    #     print w_file.read()
    finally:
        w_file.close()
    return 'set ip successful'   

def regedit_dev(dev=None):
    if not dev:
        def _register(xdev):
            return regedit_dev(xdev)
        return _register
    
class Gzip_Strio(object):  
    @classmethod
    def file2zipbuf(cls, inputfile,base64en = False):  
        buf = StringIO()  
    #     gzip.GzipFile(filename="", mode="wb", compresslevel=9, fileobj=open('sitemap.log.gz', 'wb'))
        with gzip.GzipFile(mode='wb', fileobj=buf) as f:
            f.write(open(inputfile, 'rb').read()) 
        if base64en:
            import base64
            return base64.encodestring(buf.getvalue())  
        else:    
            return buf.getvalue()  
    
    @classmethod 
    def zipbuf2file(cls, zipbuf, fn_out,base64en = False): 
        if base64en: 
            import base64
            buf = StringIO(base64.decodestring(zipbuf))
        else:
            buf = StringIO(zipbuf)
        with gzip.GzipFile(mode='rb', fileobj=buf) as f, open(fn_out, 'wb') as f_out:
            r_data = f.read()  
            f_out.write(r_data) 
        
    @classmethod 
    def zipfile2filebuf(cls, gzipfile):  
        with open(gzipfile, 'rb') as rf,gzip.GzipFile(mode='rb', fileobj=StringIO(rf.read()) ) as f:
            r_data = f.read()  
        return r_data 
     
    @classmethod            
    def compress_file(cls, fn_in, fn_out):  
        with open(fn_in, 'rb') as f_in, gzip.open(fn_out, 'wb') as f_out:
            f_out.writelines(f_in)
#         f_in = open(fn_in, 'rb')
#         f_out = gzip.open(fn_out, 'wb')  
#         f_out.writelines(f_in)  
#         f_out.close()  
#         f_in.close()  
        
    @classmethod      
    def uncompress_file(cls, fn_in, fn_out):  
        f_in = gzip.open(fn_in, 'rb')  
        f_out = open(fn_out, 'wb')  
        file_content = f_in.read()  
        f_out.write(file_content)  
        f_out.close()  
        f_in.close()  
      
class Argumentparser(object):
    def __init__(self,  file_path):
        self.get_connect_info(file_path)
    def write_parser(self, file_path, sec, opt, value):
        conf = ConfigParser.ConfigParser()
        conf.read(file_path)
        if not conf.has_section(sec):
            conf.add_section(sec)
        conf.set(sec, opt, value)
        conf.write(open(file_path, "w"))
        self.get_connect_info(file_path)
        if str(value) == conf.get(sec, opt):
            return 0
        else:
            return 'unknow error'
    
    def set_interval(self, value, file_path=yj_global.CONFIGPATH):
        if int(value) < 5:
            value = 5
        return self.write_parser(file_path, 'Mqtt', 'publish_interval', str(value))    
    
    def set_runstate(self, file_path=yj_global.CONFIGPATH):    
        conf = ConfigParser.ConfigParser()
        conf.read(file_path)
        value = long(conf.get('Mqtt', 'runstate')) + 1
        return self.write_parser(file_path, 'Mqtt', 'runstate', str(value))
  
    def set_connect_info(self, file_path=yj_global.CONFIGPATH, \
                         hostname='192.168.1.160', \
                         port='1883', \
                         username='yj', \
                         password='yj12345', \
                         topic='things', \
                         runstate='1', \
                         interfaces='test.txt', \
                         client_ip_acquisition='static', \
                         client_ip='192.168.1.188', \
                         client_netmask='255.255.255.0', \
                         client_gateway='192.168.1.1', \
                         _client_id='TG-200-20160001'):
        if (not os.path.exists(file_path)): 
            f = open(file_path, 'w') 
            f.write('''[Mqtt]
                    hostname = 192.168.1.180
                    port = 1883
                    _client_id = TD1000-xxxx
                    keepalive = 60
                    will = 1
                    username = yj
                    password = yj12345
                    subscribe_qos = 2
                    topic = things
                    pub_qos = 1
                    pub_retain = 0
                    runstate = 1
                    publish_interval = 4
                    status = offline'''
                    )   
            f.close()  
        pattern_1 = re.compile(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$')
        match_1 = pattern_1.match(hostname)
        if(match_1 == None or not str(port).isdigit()):
            print "wrong data"
            return "wrong data"
        conf = ConfigParser.ConfigParser()
        conf.read(file_path)
        conf.set('Mqtt', 'hostname', hostname)
        conf.set('Mqtt', 'port', port)
        conf.set('Mqtt', 'username', username)
        conf.set('Mqtt', 'password', password)
        conf.set('Mqtt', 'topic', topic)
#         conf.set('Mqtt', 'runstate', runstate)
    #     conf.set('Mqtt', '2._client_id',_client_id)
        conf.write(open(file_path, "w"))
        ret = set_ipconf(interfaces, client_ip_acquisition, client_ip, client_netmask, client_gateway) 
        if(ret != 'set ip successful'):
            print 'set connect Mqtt failed'
            return 'set connect Mqtt failed'
        print "set connect Mqtt successful"   
        return 0

    def get_connect_info(self, file_path=yj_global.CONFIGPATH):
        import shutil
        #shutil.copyfile(sys.path[0] + '/connect_info.ini', sys.path[0] + '/connect_info.back')   
        conf = ConfigParser.ConfigParser()
        conf.read(file_path)
        sec = conf.sections()
        self.eviron = {}
        for ts in sec:
            self.eviron.update(dict(conf.items(ts)))
        self.eviron.update({"internal_ip": get_ip_address(self.eviron["internal_if"])})
        self.eviron.update({"external_ip": get_ip_address(self.eviron["external_if"])})

    def sys_setget(self, cmd, data=''):
        if platform.system() == 'Linux':
            interfaces = '/etc/network/interfaces'
        else:
            interfaces = 'test.txt'    
        if cmd == r'init/set.do':
            for key in data:
                data[key] = str(data[key])
            if isinstance(data, dict) and data.has_key('_interface_inet'):
                if data['_interface_inet'] == r'static':
                    if data.has_key('_server_ip')\
                     and data.has_key('_server_port')\
                     and data.has_key('_username')\
                     and data.has_key('_password')\
                     and data.has_key('_server_name')\
                     and data.has_key('_client_ip')\
                     and data.has_key('_client_netmask')\
                     and data.has_key('_client_gateway'):
                        ret = self.set_connect_info(\
                                                     hostname=data['_server_ip'], \
                                                     port=data['_server_port'], \
                                                     username=data['_username'], \
                                                     password=data['_password'], \
                                                     topic=data['_server_name'], \
                                                     runstate='1', \
                                                     interfaces=interfaces, \
                                                     client_ip_acquisition='static', \
                                                     client_ip=data['_client_ip'], \
                                                     client_netmask=data['_client_netmask'], \
                                                     client_gateway=data['_client_gateway'])
                    else:
                        return 'wrong data'
                elif data['_interface_inet'] == r'dhcp':
                        if data.has_key('_server_ip')\
                         and data.has_key('_server_port')\
                         and data.has_key('_username')\
                         and data.has_key('_password')\
                         and data.has_key('_server_name'):
                            ret = self.set_connect_info(\
                                                     hostname=data['_server_ip'], \
                                                     port=data['_server_port'], \
                                                     username=data['_username'], \
                                                     password=data['_password'], \
                                                     topic=data['_server_name'], \
                                                     runstate='1', \
                                                     interfaces=interfaces, \
                                                     client_ip_acquisition='dhcp')
                        else:
                            return 'wrong data'
                else:
                    return 'wrong data'
            if ret == 0:
                os.system('reboot')
                return 0                 
            else:
                return ret
        elif cmd == r'init/get.do':
            ret = self.get_connect_info()
            return ret 
          
        elif cmd == r'manager/set_interval.do':
            if isinstance(data, dict) and data.has_key('_interval'):
                ret = self.set_interval(data['_interval'])
                return ret 
            else:
                return 'wrong data'
      
        elif cmd == r'manager/set_system_time':
            if isinstance(data, dict) and data.has_key('date') and data.has_key('time'):
                # date -s "2008-08-08 12:00:00" #date -s 05/10/2009  #date -s 05/10/2009
                if platform.system() == 'Linux':
                    if  re.match(r'\d{2}/\d{2}/\d{4}', data['date']) and re.match(r'\d{2}:\d{2}:\d{2}', data['time']):               
                        os.system('date' + ' -s' + '\" ' + data['date'] + ' ' + data['time'] + '\"')
                        return 0
                    else:
                        return "wrong format"
                else:
                    if  re.match(r'\d{2}/\d{2}/\d{4}', data['date']) and re.match(r'\d{2}:\d{2}:\d{2}', data['time']):              
                        return 0
                    else:
                        return "wrong format"
            else:
                return 'wrong data'
        elif cmd == r'manager/get_suppot_devlist':
            ret = 'BACnet'
            return ret

        elif cmd == r'manager/dev/list.do':
            return self.devname
                
        elif cmd == r'manager/dev/update.do':
            ret = []
            if isinstance(data,dict):
                try:
                    self.mkreqob(data['_devaddr'].encode("utf-8"))
                    ret = self.writedevname(data['_devid'].encode("utf-8"),data['_devaddr'].encode("utf-8"))
                except Exception,e:
                    ret = e.message
            elif isinstance(data,list):
                for da in data:
                    try:
                        self.mkreqob(da['_devaddr'].encode("utf-8"))
                        ret.append(self.writedevname(da['_devid'].encode("utf-8"),da['_devaddr'].encode("utf-8"))) 
                    except Exception,e:
                        ret.append(e.message) 
            return ret
            
        elif cmd == r'manager/dev/delete.do':
            ret = []
            if isinstance(data,dict):
                try:
                    ret = self.deldevname(data['_devid'].encode("utf-8"))
                except Exception,e:
                    ret = e.message
            elif isinstance(data,list):
                for da in data:
                    try:
                        ret.append(self.deldevname(da['_devid'].encode("utf-8"))) 
                    except Exception,e:
                        ret.append(e.message) 
            return ret            
                            
        elif cmd == r'yjiongcmd':
            if platform.system() == 'Linux':
    #             import commands
                ret = os.popen(data).read()
    #             ret = commands.getoutput(data)
            else:
                ret = os.popen(data).read().decode('gb2312') 
            return [ret]
        elif cmd == r'manager/update_drive' or cmd == r'manager/update_tg200':
            if isinstance(data, str) and  re.compile(r'http://.*').match(data):
                if cmd == r'manager/update_drive':
                    ret = os.system(sys.path[0] + '/./update.sh ' + data + '/drive.tar.gz')
                else:
                    ret = os.system(sys.path[0] + '/./update.sh ' + data + '/tg200.tar.gz')
            else:
                return 'wrong data'
            return ret   
        return 'unknow cmd'

    def get_date_from_msg(self, rcv_msg):
        if(rcv_msg["header"]["msgtype"] == "request"):
            return rcv_msg["request"]['cmd'], rcv_msg["request"]['data']
        else:
            return 0
            
    def make_pub_resp_msg(self, rcv_msg, pdata, statuscode): 
        requ_dic = {}
#         self.get_connect_info(yj_global.CONFIGPATH)
        try:
            if rcv_msg["request"].has_key("return"):
                for i in  range(len(rcv_msg["request"]["return"])):
                    requ_dic.update({rcv_msg["request"]["return"][i]:rcv_msg["request"][rcv_msg["request"]["return"][i]]})
        except:
            requ_dic = {"requestid":rcv_msg["request"]["requestid"]}
        tdic = {"header": 
                        { 
                        "msgtype": "response",
                        "from": {
                                "_devid": self.eviron['_client_id'],
                                "_model": "TD1000",
                                "_version": "v1.0",
                                "_runstate":self.eviron['runstate']
                                }
                        },
                "request": requ_dic,
                "response": 
                        {
                            "timestamp": int(time.time()),
#                             "local_cpu_temp":os.popen('cat /sys/devices/virtual/thermal/thermal_zone0/temp').read().split('\n')[0],
                            "statuscode": statuscode,
                            "data":pdata
                        }
                }
        return tdic
    
    def make_pub_update_msg(self, updata, cmd='do/auto_up_data'):
        uptdic = {"head": 
                        { 
                        "msgtype": "update",
                        "from": {
                                "_devid": self.eviron['_client_id'],
                                "_model": "TD1000",
                                "_version": "v1.0",
                                "_runstate":self.eviron['runstate']
                                }
                        },
                "request": 
                        {
                            "timestamp": int(time.time()),
#                             "local_cpu_temp":os.popen('cat /sys/devices/virtual/thermal/thermal_zone0/temp').read().split('\n')[0],
    #                         "requestid": str(uuid.uuid1()),
                            "cmd":cmd,
                            "data":updata
                        }
                }
        return uptdic
    
    def error_code(self, retdata):
        error_dic = {
                     '0':0,
                     'unknow cmd':101,
                     'permission denied':102,
                     'device unknow cmd':103,
                     'wrong data':104,
                     'unknow variable':105,
                     'read only':106,
                     'write only':107,
                     'unknow error':201,
                     'update faile':256,
                     'offline':301,
                     'time out':302,
                     'already init':401,
                     'commid not on list':402,
                     'unknow device':403,
                     'unknow devid':404,
                     'No Device remove':405,
                     'the port already binding device':406,
                     'devcomm_addr is not a communication address':407,
                     'commid not set device':408,
                     'port not binding':409,
                     'devcomm_addr is not a devcomm_addr':410,
                     'device_addr is not a device_address':411,
                     'wrong format':412,
                     'write_contrl_byte Failed':523,
                     'Buy Electricity Failed':524,
                     'Write_metering Failed':525
                     } 
        if error_dic.has_key(str(retdata)):
            return error_dic[str(retdata)] 
        else:
            if isinstance(retdata, int):
                return int(retdata)
            return 201
# 
if __name__ == "__main__":
#     
#     print Gzip_Strio.file2zipbuf(sys.path[0] + '/260002.xls')
#     fbuf = Gzip_Strio.file2zipbuf(sys.path[0] + '/260002.xls')
#     Gzip_Strio.zipbuf2file(fbuf,'dsfadf.xls')
#     print Gzip_Strio.zipfile2filebuf('init_set.py.gz')
#     Gzip_Strio.compress_file(sys.path[0] + '/260002.xls', 'xxx.gz')
    b64gzstr = '''H4sIAEzV9VgC/+2ZS28cRRDHe2bfb+/7/d6EOCSQXecJArHGCUqUxJGTmMcBEScmhBgTGQcBh4CQ
4MABEHwBEBJ3LkgckHKEAwIhJK7hQ3AjLDXVvTXV62bWgUgoKLNq71b9u/o3M9XV8/BPP6Zvff5V
5TcxsT0ufOLPUUQEmc9SDbcZAfpo5Pwcfzvb6P52T223oTn580Hu/NAC0Jych6CFoUWgRaHFoMWh
JaAloaVU3u9v9/a2JF6Fz6ZoiaNiHb43xJviTrYCzJjxWNaUvi8MVo788uXPFl8vngb6hrgqVnA/
roo73TLCtvjxbCfGhvZRVdy17U75d3v7N/xIGBbyYEB8Vv4h9P1N+AkLwWNwVt+AT/PRc73DauV/
xu6LivjOob39sUp0wBpC7q6IC2JN3BI59P0+arHRb7bu+//f/k9tuGq8I0bb/X4Prh57nYsKXGXO
vrS6utn/Q3wR3AT7EFamdVwswIXIOg2z6hWxCtchawm+LotzsDBdg1/RsWMoLsFnA36/Bp+wsBYg
ZBMa6zkPvZ6EdW0VhLE7obv1YWLOMM7Pizizr0GHK7AurcNeQ9RxcRr6L4lnYbl0fp8R52HYltgH
d0Mo7xLz0H8ddsJZR51O6zDGdRhl1jmSeVzk1gBwYdqQfW954C3Pecv74fI9IS+i5B5Q2umwuOWA
FvFgxoc0ZZT+tA6DaR0Mx7EMv06CfNQ978tbdnMZ16TrcKJnvUfoe8sDb3nK7u33lg94ywe95UPe
8mFv+YgjD1E8Cef8KdOEHuKEHuJpXYN5e3liQgeEdQwFp+w8h+t7ywNveQ5uMqxd4hRg17Ae90J9
biJ1VdulPbjnsyIvrFO479ND8CgW1F3IJWc66vthKIshloV+WraUhdco/WkdBtM6zImsc4jnQT4H
B3MW/g6hHTXscMHpuOhxLvRd3+a4/e12HGy349zWObCl0odY6fqZn6j0vx+h7y0PvOU5fAiSN0sz
2s1SzC7R5XCH+v4GXBedG0w7yfzO41TYnlG/R/RM7fgsg882+HwGn9/gCxh8QYMvZPCFDb6IwRc1
+GIGX9zgSxh8SYMvZfDNGHxpgy9j8GUNvpzBlzf4CgZf0eArGXxlg69i8FUNvprBVzf4GgZfc8J3
G7M23qQ1fq0jLVs1aflUk5ZfNWkFVJNWUDVphVS7YUfVK6OxZqm5PrZs9ZJB9vQxuoUsZ7RvIZcW
spL0ABcUlg2NKDYqURVrIyVGFqfYSAmSxik2owSJYhPFh0pSxfqQEieLU3zYN0oap/gYJUoUH1H8
6tFWxvqRkiCLU/xISZLGKX5GSRLFT5SAeoCXsQGkJMnilABSMqRxSoBRMkQJEEW+QKyo2CBSUmTZ
8k2i6smzH9QoQaRkJrIfJEoIlYaKDSElTRanhLTshzRKiFHc7IeIEkalo2LDSMmQxSlhLfthjRJm
FDf7YaLg05HYqWIjSMmSxSkRLfsRjRJhFDf7EaLIdWC3ipVVmiPLoeSpJ89LVKPIWVSYyEuUKDFU
HlKxMaQUyOKUmJaXmEaJMYqblxhR4qgMVGwcKUWyOCWu5SWuUeKM4uYlTpQEKgdVbAIpJbI4JaHl
JaFREozi5iVBFFlJj6jYJFLKZHFKUqvKpEZJMopblUmipNRLfhmbQkqFLE5JIaVAGqekGKVAlBRR
5L3OvIqVdzlVsjhF3tlUSOOUGUapEGWGKGlUjqnYNFJqZHFKGikN0jglzSgNoqSJIivphIrNIKVO
FqdkkNIhjVMyjNIhSoYoWVROq9gsUhpkcUoWKTtJ45Qso+wkSpYo8lXOkorNIaVJlkNpUU9e+zmk
tBUlhxR7ovZzRMmjsqxi80jpkMUpea328xolzyhu7eeJIo/yORVbQEqXLE4paLVf0CgFRnFrv0CU
IirPq9giUnpkcUpRq/2iRikyilv7RaLIJ4kVFVtCyg6ybJVN2ZPnpYSUBxSlxChu7ZeIUkblRRVb
Rsouspy4WerJKWXtWMr0ryKe/TJRKqi8rGIrSNlNFqdUtOxXNEqFUdzsV4gi3+Kvq9gqUh4ki1Oq
WvarGqXKKG72q0SpobKhYmtI2UMWp9S07Nc0So1R3OzXiFJH5XUVW0fKXrJsdR2VPXle6lr264zi
Zr9OlAYqb6nYBlIeJotTGlpeGhqlwSjuyt8gShOVGyq2iZR9ZHFKU8tLU6M0GcVd+ZtI+VUsiBNw
GVuzr8Fc/ucf5ympZXhyaht8HYOva/D1JnzOccrX5u+q42zhGemTxc9IS5tDLe2MtNgZca9SLTrv
bVTeV7FtpAzIcihz1JPPobY2U9tIiU7UdpsoHVQ+ULEdpOwni1M62hzqaJQOo7i13SFKF5UPVWwX
KQfI4pSuNoe6GqXLKG5td4nSQ+UTFdtDykGyOKWn5aWnUXqM4tZ2T83UpFiBgc7QnHvczoivMYlP
sKfx6H/xv8O/ALFWu2cAIgAA'''    
#     Gzip_Strio.zipbuf2file(b64gzstr, 'testt.xls', 1)
    myaum = Argumentparser("./config.ini")
    import pprint
    pprint.pprint(myaum.eviron)

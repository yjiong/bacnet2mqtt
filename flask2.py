#!/usr/bin/python
# -*- utf-8 -*-
from flask_socketio import SocketIO
from flask import Flask, render_template
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@socketio.on('message')
def handle_message(message):
    print('received message: ' + message)

#The above example uses string messages. Another type of unnamed events use JSON data:

@socketio.on('json')
def handle_json(json):
    print('received json: ' + str(json))

#The most flexible type of event uses custom event names. The message data for these events can be string, bytes, int, or JSON:

@socketio.on('my event')
def handle_my_custom_event(json):
    print('received json: ' + str(json))

#Custom named events can also support multiple arguments:

# @socketio.on('my event')
# def handle_my_custom_event(arg1, arg2, arg3):
#     print('received args: ' + arg1 + arg2 + arg3)

#Named events are the most flexible, as they eliminate the need to include additional metadata to describe the message type.

#Flask-SocketIO also supports SocketIO namespaces, which allow the client to multiplex several independent connections on the same physical socket:

@socketio.on('my event', namespace='/test')
def handle_my_custom_namespace_event(json):
    print('received json: ' + str(json))
def my_function_handler(data):
    pass

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('my event')
def my_event(message):
    print(message['data'])
    
#socketio.on_event('my event', my_function_handler, namespace='/test')

#Clients may request an acknowledgement callback that confirms receipt of a message. Any values returned from the handler function will be passed to the client as arguments in the callback function:

# @socketio.on('my event')
# def handle_my_custom_event(json):
#     print('received json: ' + str(json))
#     return 'one', 2


if __name__ == '__main__':
    socketio.run(app)
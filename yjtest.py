# -*- coding:utf-8 -*-
#encoding:utf-8
import time,sys,os,configparser,ConfigParser,requests,re
from docutils.nodes import target
def consumer():
    r = ''
    while True:
        n = yield r
        if not n:
            return
        print('[CONSUMER] Consuming %s...' % n)
        time.sleep(1)
        r = '200 OK'

def produce(c):
    c.next()
    n = 0
    while n < 5:
        n = n + 1
        print('[PRODUCER] Producing %s...' % n        r = c.send(n)
        print('[PRODUCER] Consumer return: %s' % r)
    c.close()


# import gevent
# from gevent.queue import Queue
#
# tasks = Queue()
# def worker(n):
#     while not tasks.empty():
#         task = tasks.get()
#         print('Worker %s got task %s' % (n, task))
#         gevent.sleep(0)
#     print('Quitting time! by %s'%n)
#
# def boss():
#     for i in xrange(1,25):
#         tasks.put_nowait(i)
#
# gevent.spawn(boss).start()
#
# gevent.joinall([
#     gevent.spawn(worker, 'aaa'),
#     gevent.spawn(worker, 'bbb'),
#     gevent.spawn(worker, 'ccc'),
# ])

class DynApp(object):
    def __init__(self,device_path = sys.path[0] + '/device'):
        pass

    @staticmethod
    def registerdev(com):
        def call(cls):
            setattr(DynApp,cls().device_type,cls(com))
            return cls
        return call

@DynApp.registerdev('COM0')
class DevObj(object):
    def __init__(self,com = ''):
        self.wincom = com
        self.defaultport = '/dev/ttyUSB0'
        self.set_config()
    @classmethod
    def set_config(cls):
        cls.baudrate = 1200
        cls.bytesize = 8
        cls.parity = 'E'
        cls.stopbits = 1
        cls.timeout = 1
        cls.device_type = 'mydevice_name'
        cls.location = 'xindong'
myapp = DynApp()


if __name__=='__main__':
#     print myapp.mydevice_name.wincom
#     print os.listdir(sys.path[0] + '/device')
#     conf = configparser.ConfigParser()
#     conf.read('devname.ini')
#     conf.add_section("无锡")
#     conf.write(open('devname.ini','w'))
#     from io import open
#     fp = open('devname.ini','w')
#     fp.write("[{0}]\n".format(u'无锡'))
#     fp.write("[%s]\n" % u'上海')
#     print __name__
#     print dir(sys.modules.get('__main__'))
#     str = requests.get("http://city.ip138.com/ip2city.asp").content.decode('cp936').encode('utf-8')
#     ip = re.search(r'(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})',str).group()
#     print ip

    p=os.path.join(sys.path[0],'dfs')



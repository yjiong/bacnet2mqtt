#!/usr/bin/python
# -*- coding: utf_8 -*-
# encoding: UTF-8
import asyncore
import ConfigParser
import logging
import os, threading, sys, json
import signal
import time
import traceback

from bacNet.task import TaskManager

# some debugging
_log = logging.getLogger(__name__)

# globals
running = 1
taskManager = None
deferredFns = []
sleeptime = 0.0
CONFIGPATH = sys.path[0] + '/config.ini'

try:
    if not os.path.exists(CONFIGPATH) or os.path.getsize(CONFIGPATH) < 200L: 
        import shutil
#             print 'copying'
        shutil.copyfile(sys.path[0] + '/connect_info.back',CONFIGPATH)
except:
    pass
#
#   run
#

SPIN = 1.0

def run(yjapp,spin=SPIN):
    _log.debug("run spin=%r", spin)
    global running, taskManager, deferredFns, sleeptime
    # reference the task manager (a singleton)
    taskManager = TaskManager()

    # count how many times we are going through the loop
    loopCount = 0

    rc = 1
    running = 0
    while running == 0:
#       _log.debug("time: %r", time.time())
        loopCount += 1
        # get the next task
        task, delta = taskManager.get_next_task()
        if(rc != 0):
            try:
                yjapp.connect() 
            except:
                pass
            if yjapp.loop() == 0:
                online = json.dumps((yjapp.make_pub_update_msg('1', 'push/state.do')),ensure_ascii=False,indent=1,encoding='gbk')   
                deferred(yjapp.publish,online,True) 
                deferred(yjapp.subscribe)  
            time.sleep(1)
#             handle_config.set_on_offline(CONFIGPATH,'offline')
        try:
            rc = yjapp.loop()
            # if there is a task to process, do it
            if task:
                # _log.debug("task: %r", task)
                taskManager.process_task(task)

            # if delta is None, there are no tasks, default to spinning
            if delta is None:
                delta = spin

            # there may be threads around, sleep for a bit
            if sleeptime and (delta > sleeptime):
                time.sleep(sleeptime)
                delta -= sleeptime

            # if there are deferred functions, use a small delta
            if deferredFns:
                delta = min(delta, 0.001)
#           _log.debug("delta: %r", delta)

            # loop for socket activity
            asyncore.loop(timeout=delta, count=1)

            # check for deferred functions
            while deferredFns:
                # get a reference to the list
                fnlist = deferredFns
                deferredFns = []
                # call the functions
                for fn, args, kwargs in fnlist:
                    # _log.debug("call: %r %r %r", fn, args, kwargs)
                    fn( *args, **kwargs)
                
                # done with this list
                del fnlist
                
        except KeyboardInterrupt:
            _log.info("keyboard interrupt")
            running = 1
        except Exception as e:
            _log.exception("an error has occurred: %s", e)
            
    running = 1

#
#   run_once
#

def run_once():
    """
    Make a pass through the scheduled tasks and deferred functions just
    like the run() function but without the asyncore call (so there is no 
    socket IO actviity) and the timers.
    """
    _log.debug("run_once")
    global taskManager, deferredFns

    # reference the task manager (a singleton)
    taskManager = TaskManager()

    try:
        delta = 0.0
        while delta == 0.0:
            # get the next task
            task, delta = taskManager.get_next_task()
            _log.debug("    - task, delta: %r, %r", task, delta)

            # if there is a task to process, do it
            if task:
                taskManager.process_task(task)

            # check for deferred functions
            while deferredFns:
                # get a reference to the list
                fnlist = deferredFns
                deferredFns = []

                # call the functions
                for fn, args, kwargs in fnlist:
                    _log.debug("    - call: %r %r %r", fn, args, kwargs)
                    fn( *args, **kwargs)

                # done with this list
                del fnlist

    except KeyboardInterrupt:
        _log.info("keyboard interrupt")
    except Exception as e:
        _log.exception("an error has occurred: %s", e)

#
#   stop
#

def stop(*args):
    """Call to stop running, may be called with a signum and frame 
    parameter if called as a signal handler."""
    _log.debug("stop")
    global running, taskManager

    if args:
        sys.stderr.write("===== TERM Signal, %s\n" % time.strftime("%d-%b-%Y %H:%M:%S"))
        sys.stderr.flush()

    running = False

    # trigger the task manager event
    if taskManager and taskManager.trigger:
        taskManager.trigger.set()

# set a TERM signal handler
if hasattr(signal, 'SIGTERM'):
    signal.signal(signal.SIGTERM, stop)

#
#   print_stack
#

def print_stack(sig, frame):
    """Signal handler to print a stack trace and some interesting values."""
    _log.debug("print_stack, %r, %r", sig, frame)
    global running, deferredFns, sleeptime

    sys.stderr.write("==== USR1 Signal, %s\n" % time.strftime("%d-%b-%Y %H:%M:%S"))

    sys.stderr.write("---------- globals\n")
    sys.stderr.write("    running: %r\n" % (running,))
    sys.stderr.write("    deferredFns: %r\n" % (deferredFns,))
    sys.stderr.write("    sleeptime: %r\n" % (sleeptime,))

    sys.stderr.write("---------- stack\n")
    traceback.print_stack(frame)

    # make a list of interesting frames
    flist = []
    f = frame
    while f.f_back:
        flist.append(f)
        f = f.f_back

    # reverse the list so it is in the same order as print_stack
    flist.reverse()
    for f in flist:
        sys.stderr.write("---------- frame: %s\n" % (f,))
        for k, v in f.f_locals.items():
            sys.stderr.write("    %s: %r\n" % (k, v))

    sys.stderr.flush()

# set a USR1 signal handler to print a stack trace
if hasattr(signal, 'SIGUSR1'):
    signal.signal(signal.SIGUSR1, print_stack)

#
#   deferred
#

def deferred(fn, *args, **kwargs):
    # _log.debug("deferred %r %r %r", fn, args, kwargs)
    global deferredFns

    # append it to the list
    deferredFns.append((fn, args, kwargs))

#
#   enable_sleeping
#

def enable_sleeping(stime=0.001):
    _log.debug("enable_sleeping %r", stime)
    global sleeptime

    # set the sleep time
    sleeptime = stime
    
def yj_debugging(leve = 20):
    """Function for attaching a debugging logger to a class or function."""
    # create a logger for this object
    import logging.handlers
    def call(obj):
        logger = logging.getLogger(obj.__module__ + '.' + obj.__name__)
        fh = logging.handlers.RotatingFileHandler(os.path.join(sys.path[0] , obj.__name__+'.log'),  maxBytes=1024*1024*50, backupCount=3, encoding='utf-8')
        ch = logging.StreamHandler()
        formats = logging.Formatter('%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s')
        fh.setFormatter(formats)
        ch.setFormatter(formats)
        logger.setLevel(leve)
        logger.addHandler(fh)
        logger.addHandler(ch)
    #     filter=logging.Filter(”abc.xxx”)
        # make it available to instances
        obj._logger = logger
        obj._debug = logger.debug
        obj._info = logger.info
        obj._warning = logger.warning
        obj._error = logger.error
        obj._exception = logger.exception
        obj._fatal = logger.fatal
        return obj    
    return call
            

#!/usr/bin/python
# encoding:UTF-8
# from curses.ascii import isdigit
import threading, sys, time, json, re, codecs , os
import traceback  # ,collections
import logging
from bacNet.apdu import WhoIsRequest, IAmRequest, ReadPropertyMultipleRequest, WritePropertyRequest, ReadPropertyRequest, \
 PropertyReference, ReadAccessSpecification, Error, AbortPDU, ReadPropertyMultipleACK, SimpleAckPDU
from bacNet.app import LocalDeviceObject, BIPSimpleApplication, ReadPropertyACK
from bacNet.basetypes import ServicesSupported, PropertyIdentifier, PriorityArray, ShedLevel, DateTime, DeviceObjectPropertyReference
from bacNet.consolelogging import ConfigArgumentParser
from bacNet.constructeddata import Array, Any
from bacNet.debugging import  ModuleLogger
from bacNet.errors import DecodingError
from bacNet.object import get_object_class, get_datatype
from bacNet.pdu import Address, GlobalBroadcast
from bacNet.primitivedata import Unsigned, Null, Atomic, Integer, Real
import init_set
from yj_global import run, deferred
import yj_global, inspect
from flask import Flask, render_template
from flask import request
import gevent
from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket import WebSocketError
from gevent.monkey import patch_all
from ConfigParser import ConfigParser
# from werkzeug.contrib.cache import _items
patch_all()
# logging.basicConfig(level=logging.DEBUG)
# log = logging.getLogger(__name__)

try:
    import paho.mqtt.client as mqtt
except ImportError:
    cmd_subfolder = os.path.realpath(os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], "../src")))
    if cmd_subfolder not in sys.path:
        sys.path.insert(0, cmd_subfolder)
# some debugging
_debug = 0
_log = ModuleLogger(globals())
# globals
this_device = None
this_console = None
def trace():
#     import traceback,inspect
    print inspect.stack()[0][3]
    s = traceback.extract_stack()
    print "%-80s%-10s%-30s%-60s" % ('path', 'line', 'funname', 'selffun')
    for ss in s:
        print '{0:<80}{1:<10}{2:<30}{3:<60}'.format(ss[0], ss[1], ss[2], ss[3])
@yj_global.yj_debugging(logging.WARNING)
class YjApp(BIPSimpleApplication, mqtt.Mosquitto, init_set.Argumentparser, Flask):
    def __init__(self, device,  configpath, import_name):
        init_set.Argumentparser.__init__(self, configpath)
        BIPSimpleApplication.__init__(self, device, self.eviron["internal_ip"] + '/24')
        mqtt.Mosquitto.__init__(self, 'yjiong' + self.eviron['_client_id'])
        Flask.__init__(self, import_name)
        self.readdevname()
        self.on_message = self.mqtt_message
        self.username_pw_set(self.eviron['_username'], self.eviron['_password'])
        self.willmsg = json.dumps(self.make_pub_update_msg('0', 'push/state.do'))
        self.will_set(self.eviron['_server_name'] + '/' + self.eviron['_client_id'], self.willmsg, 1, True)
        self.subTopic = (self.eviron['_client_id'] + '/' + self.eviron['_server_name']).encode("utf-8")
        self.pubTopic = (self.eviron['_server_name'] + '/' + self.eviron['_client_id']).encode("utf-8")
        self.whoislist = {}
        self.cmd_msg = {}
        self.apdudic = {}
        self.users = set()
        # keep track of requests to line up responses
        self._request = None
    def readdevname(self):
        if not os.path.exists(sys.path[0] + '/devname.ini'):
            with open(sys.path[0] + '/devname.ini', 'w') as f:
                conf = ConfigParser()
                conf.add_section('bacNetdev')
                conf.write(f)
                self.devname = dict(conf.items('bacNetdev'))
        else:
            conf = ConfigParser()
            with codecs.open(sys.path[0] + '/devname.ini', encoding='utf-8') as f:
                conf.readfp(f)
    #             ini_obj = type('devname', (), dict(conf.items('bacNetdev')))
                setattr(self, 'devname', dict(conf.items('bacNetdev')))

    def writedevname(self, opt, val):
        conf = ConfigParser()
        conf.read(sys.path[0] + '/devname.ini')
        if val not in self.devname.values():
            conf.set('bacNetdev', opt, val)
            conf.write(open(sys.path[0] + '/devname.ini', 'w'))
            self.readdevname()
            self.set_runstate()
            return u"(_devid:%s)更新成功" % unicode(opt, 'utf-8')
        else:
            raise RuntimeError, u'(_devaddr:%s)已经存在' % val

    def deldevname(self, opt):
        conf = ConfigParser()
        conf.read(sys.path[0] + '/devname.ini')
        if unicode(opt, 'utf-8') in self.devname.keys():
            conf.remove_option('bacNetdev', opt)
            conf.write(open(sys.path[0] + '/devname.ini', 'w'))
            self.readdevname()
            self.set_runstate()
            return u"(_devid:%s)删除成功" % unicode(opt, 'utf-8')
        else:
            raise RuntimeError, u'(_devid:%s)不存在' % unicode(opt, 'utf-8')

    def connect(self):
        return mqtt.Mosquitto.connect(self, self.eviron['_server_ip'], int(self.eviron['_server_port']), int(self.eviron['_keepalive']))

    def publish(self, payload=None, retain=False):
        return mqtt.Mosquitto.publish(self, self.pubTopic, payload=payload, qos=1, retain=retain)

    def subscribe(self):
        return mqtt.Mosquitto.subscribe(self, self.subTopic, qos=int(self.eviron['subscribe_qos']))

    def cmdackpub(self, cmd_key, retcontent=None, errcode=0):
        try:
            if cmd_key in self.cmd_msg:
                msgd = self.cmd_msg.pop(cmd_key)
                send = self.make_pub_resp_msg(msgd, retcontent, errcode)
                if 'websocket' in msgd:
                    msgd['websocket'].send(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
                else:
                    self.publish(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
            elif cmd_key == 'write':
                for key in self.cmd_msg.keys():
                    if re.search(r',write$', key) != None:
                        msgd = self.cmd_msg.pop(key)
                        send = self.make_pub_resp_msg(msgd, retcontent, errcode)
                        if 'websocket' in msgd:
                            msgd['websocket'].send(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
                        else:
                            self.publish(json.dumps(send, ensure_ascii=False, indent=1, encoding='utf-8'))
        except Exception:
            self._error(sys.exc_info()[1])

    def mqtt_message(self, mosq, obj, msg):
        self.base_message(mosq=mosq, obj=obj, msg=msg)

    def base_message(self, mosq=None, obj=None, msg=None, wsmsg=None , wsobj=None):
        if msg or wsmsg:
            try:
                if wsmsg:
                    now_msg = json.loads(wsmsg)
                elif (msg.qos == int(self.eviron['subscribe_qos']) and not msg.retain):
                    now_msg = json.loads(msg.payload)
                cmd, data = self.get_date_from_msg(now_msg)
                if cmd in (r'do/getvar', r'do/setvar'):
                # do read write property
                    cmd_str = ''
                    if data["_devid"] in self.devname:
                        cmd_str = self.devname[data["_devid"]].encode("utf-8")
                    else:
                        cmd_str = data["_devid"].encode("utf-8")
                    if cmd_str in self.cmd_msg or cmd_str + ',write' in self.cmd_msg:
                        cmdob = u"(_devid:%s)前一个操作正在处理中,你等等再来吧!" % data["_devid"]
                        handlebusy = json.dumps(self.make_pub_resp_msg(now_msg, cmdob, 1), ensure_ascii=False, indent=1, encoding='utf-8')
                        if wsmsg:
                            wsobj.send(handlebusy)
                        else:
                            deferred(self.publish, handlebusy)
                    else:
                        if cmd == r'do/getvar':
                            self.cmd_msg[cmd_str] = now_msg
                            if wsmsg:
                                self.cmd_msg[cmd_str].update({'websocket':wsobj})
                            deferred(self.doreadpropertymultiple, cmd_str)
                            threading.Timer(10, self.cmdackpub, (cmd_str, u'哎哟,超时了!!!')).start()
                        elif data.has_key('_varvalue'):
                            self.cmd_msg[cmd_str + ',write'] = now_msg
                            if wsmsg:
                                self.cmd_msg[cmd_str + ',write'].update({'websocket':wsobj})
                            deferred(self.dowriteproperty, cmd_str, str(data['_varvalue']))
                            threading.Timer(10, self.cmdackpub, (cmd_str + ',write', u'哎哟,超时了!!!')).start()
                else:
                    ret = self.sys_setget(cmd, data)
                    self.get_connect_info()
                    if isinstance(ret, (list, dict, str, unicode, int, long, float)):
                        send_ack = self.make_pub_resp_msg(now_msg, ret, None)
                    else:
                        send_ack = self.make_pub_resp_msg(now_msg, None, self.error_code(ret))
#                     print json.dumps(send_ack,ensure_ascii=False,indent=1,encoding='gbk')
                    if wsmsg:
                        wsobj.send(json.dumps(send_ack, ensure_ascii=False, indent=1, encoding='utf-8'))
                    else:
                        self.publish(json.dumps(send_ack, ensure_ascii=False, indent=1, encoding='utf-8'))
                # self.publish(encode(send_ack))
            except:
                self._error(sys.exc_info()[1])
#                 self.publish(encode(self.make_pub_resp_msg(messdic,None,self.error_code(ret))))

    def get_whoislist(self):
        request = WhoIsRequest()
        request.pduDestination = GlobalBroadcast()
        self.request(request)

    def request(self, apdu):

        # save a copy of the request
        self._request = apdu
        # forward it along
        BIPSimpleApplication.request(self, apdu)

    def doreadproperty(self, addr, obj_type, obj_inst, prop_id, index=None):
        request = ReadPropertyRequest(
            objectIdentifier=(obj_type, obj_inst),
            propertyIdentifier=prop_id,
            )
        request.pduDestination = Address(addr)

        if index:
            request.propertyArrayIndex = int(index)
        self.request(request)

    def mkreqob(self, args):
        if isinstance(args, (str,)):
            args = re.split(',|-| |\*', args)
        if _debug: YjApp._debug("doreadpropertymultiple %r", args)
        i = 0
        i += 1
        addr = args[0]
        read_access_spec_list = []
        while i < len(args):
            obj_type = args[i]
            i += 1
            if obj_type.isdigit():
                obj_type = int(obj_type)
            elif not get_object_class(obj_type):
                raise ValueError, u"未知的对象类型"  # "unknown object type"

            obj_inst = int(args[i])
            i += 1

            prop_reference_list = []
            while i < len(args):
                prop_id = args[i]
                if prop_id not in PropertyIdentifier.enumerations:
                    break

                i += 1
                if prop_id in ('all', 'required', 'optional'):
                    pass
                else:
                    datatype = get_datatype(obj_type, prop_id)
                    if not datatype:
                        raise ValueError, u"无效的对象属性"  # "invalid property for object type"

                # build a property reference
                prop_reference = PropertyReference(
                    propertyIdentifier=prop_id,
                    )

                # check for an array index
                if (i < len(args)) and args[i].isdigit():
                    prop_reference.propertyArrayIndex = int(args[i])
                    i += 1

                # add it to the list
                prop_reference_list.append(prop_reference)

            # check for at least one property
            if not prop_reference_list:
                raise ValueError, u"缺少对象属性"  # "provide at least one property"

            # build a read access specification
            read_access_spec = ReadAccessSpecification(
                objectIdentifier=(obj_type, obj_inst),
                listOfPropertyReferences=prop_reference_list,
                )

            # add it to the list
            read_access_spec_list.append(read_access_spec)

        # check for at least one
        if not read_access_spec_list:
            raise RuntimeError, u"无效的请求参数"  # "at least one read access specification required"

        # build the request
        request = ReadPropertyMultipleRequest(
            listOfReadAccessSpecs=read_access_spec_list,
            )
        request.pduDestination = Address(addr)
        if _debug: YjApp._debug("    - request: %r", request)
        return request

    def doreadpropertymultiple(self, args):
        puargs = args
        try:
            # give it to the application
            request = self.mkreqob(args)
            self.request(request)
        except Exception, e:
            self.cmdackpub(puargs, e.message , 1)
            YjApp._exception("exception: %r", e)

    def dowriteproperty(self, args, value=0):
        """write <addr> <type> <inst> <prop> <value> [ <indx> ] [ <priority> ]"""
        args = args + ',' + str(value)
        if isinstance(args, str):
            args = re.split(',|-| |\*', args)
        YjApp._debug("do_write %r", args)

        try:
            addr, obj_type, obj_inst, prop_id = args[:4]
            if obj_type.isdigit():
                obj_type = int(obj_type)
            obj_inst = int(obj_inst)
            value = args[4]

            indx = None
            if len(args) >= 6:
                if args[5] != "-":
                    indx = int(args[5])
            if _debug: YjApp._debug("    - indx: %r", indx)

            priority = None
            if len(args) >= 7:
                priority = int(args[6])
            if _debug: YjApp._debug("    - priority: %r", priority)

            # get the datatype
            datatype = get_datatype(obj_type, prop_id)
            if _debug: YjApp._debug("    - datatype: %r", datatype)

            # change atomic values into something encodeable, null is a special case
            if (value == 'null'):
                value = Null()
            elif issubclass(datatype, Atomic):
                if datatype is Integer:
                    value = int(value)
                elif datatype is Real:
                    value = float(value)
                elif datatype is Unsigned:
                    value = int(value)
                value = datatype(value)
            elif issubclass(datatype, Array) and (indx is not None):
                if indx == 0:
                    value = Integer(value)
                elif issubclass(datatype.subtype, Atomic):
                    value = datatype.subtype(value)
                elif not isinstance(value, datatype.subtype):
                    self.cmdackpub('write', u"无效的数据类型", 1)
                    raise TypeError, "invalid result datatype, expecting %s" % (datatype.subtype.__name__,)
            elif not isinstance(value, datatype):
                self.cmdackpub('write', u"无效的数据类型", 1)
                raise TypeError, "invalid result datatype, expecting %s" % (datatype.__name__,)
            if _debug: YjApp._debug("    - encodeable value: %r %s", value, type(value))

            # build a request
            request = WritePropertyRequest(
                objectIdentifier=(obj_type, obj_inst),
                propertyIdentifier=prop_id
                )
            request.pduDestination = Address(addr)

            # save the value
            request.propertyValue = Any()
            try:
                request.propertyValue.cast_in(value)
            except Exception, e:
                YjApp._exception("WriteProperty cast error: %r", e)

            # optional array index
            if indx is not None:
                request.propertyArrayIndex = indx

            # optional priority
            if priority is not None:
                request.priority = priority

            if _debug: YjApp._debug("    - request: %r", request)

            # give it to the application
            self.request(request)

        except Exception, e:
            self.cmdackpub('write', u"无效的数据类型", 1)
            YjApp._exception("exception: %r", e)

    def confirmation(self, apdu):
#         print json.dumps(apdu.dict_contents()["listOfReadAccessResults"][0]["objectIdentifier"], indent=1)
#         print json.dumps(apdu.dict_contents()["source"], indent=1)
#         print json.dumps(apdu.dict_contents(), indent=1)

        apdudic = {}
        if _debug: YjApp._debug("confirmation %r", apdu)

        if isinstance(apdu, Error):
            self.cmdackpub('write', "error: %s" % (apdu.errorCode,), 1)
            apdudic['Error'] = apdu.errorCode
            sys.stdout.write("error: %s\n" % (apdu.errorCode,))
            sys.stdout.flush()

        elif isinstance(apdu, AbortPDU):
            apdu.debug_contents()

        elif isinstance(apdu, SimpleAckPDU):
            self.cmdackpub('write', u'写变量操作成功')
            apdudic['ack'] = 'Ack'
            sys.stdout.write("ack\n")
            sys.stdout.flush()

        elif (isinstance(self._request, ReadPropertyRequest)) and (isinstance(apdu, ReadPropertyACK)):
            datatype = get_datatype(apdu.objectIdentifier[0], apdu.propertyIdentifier)
            if _debug: YjApp._debug("    - datatype: %r", datatype)
            if not datatype:
                raise TypeError, "unknown datatype"

            # special case for array parts, others are managed by cast_out
            if issubclass(datatype, Array) and (apdu.propertyArrayIndex is not None):
                if apdu.propertyArrayIndex == 0:
                    value = apdu.propertyValue.cast_out(Unsigned)
                else:
                    value = apdu.propertyValue.cast_out(datatype.subtype)
            else:
                value = apdu.propertyValue.cast_out(datatype)
            if _debug: YjApp._debug("    - value: %r", value)
            self.whoislist[(apdu.dict_contents()["source"], apdu.dict_contents()["objectIdentifier"])] = value
#             sys.stdout.write(str(value) + 'mystrying\n')
            if hasattr(value, 'debug_contents'):
                value.debug_contents(file=sys.stdout)
            sys.stdout.flush()

        elif (isinstance(self._request, ReadPropertyMultipleRequest)) and (isinstance(apdu, ReadPropertyMultipleACK)):
            # <addr> <type> <inst> <prop> <value>
            ati = apdu.dict_contents()["source"] + ',' + apdu.dict_contents()["listOfReadAccessResults"][0]["objectIdentifier"][0] + ',' + \
            str(apdu.dict_contents()["listOfReadAccessResults"][0]["objectIdentifier"][1])  # + ',' + \
#             apdu.dict_contents()["listOfReadAccessResults"][0]["listOfResults"][0]["propertyIdentifier"]
#             print ati
#             print json.dumps(apdu.dict_contents(), indent=1)
            for cmdm in self.cmd_msg:
                if re.search(ati, cmdm) != None:
                    ati = cmdm
    #         print set(ati) <= set(re.split(',|-| |\*',self.devname.encode('utf-8')))
            # loop through the results
            for result in apdu.listOfReadAccessResults:
                # here is the object identifier
                objectIdentifier = result.objectIdentifier
                if _debug: YjApp._debug("    - objectIdentifier: %r", objectIdentifier)
                # now come the property values per object
                for element in result.listOfResults:
                    # get the property and array index
                    propertyIdentifier = element.propertyIdentifier
                    if _debug: YjApp._debug("    - propertyIdentifier: %r", propertyIdentifier)
                    propertyArrayIndex = element.propertyArrayIndex
                    if _debug: YjApp._debug("    - propertyArrayIndex: %r", propertyArrayIndex)
                    # here is the read result
                    readResult = element.readResult
#                     sys.stdout.write(propertyIdentifier)
                    if propertyArrayIndex is not None:
                        apdudic['propertyArrayIndex'] = str(propertyArrayIndex)
                        sys.stdout.write("[" + str(propertyArrayIndex) + "]")
                    # check for an error
                    if readResult.propertyAccessError is not None:
                        apdudic['propertyAccessError'] = str(readResult.propertyAccessError)
#                         sys.stdout.write(" ! " + str(readResult.propertyAccessError) + '\n')

                    else:
                        # here is the value
                        propertyValue = readResult.propertyValue
                        # find the datatype
                        datatype = get_datatype(objectIdentifier[0], propertyIdentifier)
                        if _debug: YjApp._debug("    - datatype: %r", datatype)
                        if not datatype:
                            apdudic[propertyIdentifier] = propertyValue.dict_contents()
                            #print "is not datatype"
                            #print propertyIdentifier,propertyValue.dict_contents()
                            continue
#                             raise TypeError, "unknown datatype"
                        # special case for array parts, others are managed by cast_out
                        if issubclass(datatype, Array) and (propertyArrayIndex is not None):
                            if propertyArrayIndex == 0:
                                value = propertyValue.cast_out(Unsigned)
                            else:
                                value = propertyValue.cast_out(datatype.subtype)
                        else:
                            value = propertyValue.cast_out(datatype)

                        if isinstance(value, (PriorityArray, ShedLevel, DateTime, DeviceObjectPropertyReference)):
#                            print 'my print',dir(value[1]),'\n',value,'\n',value.dict_contents()
                            value = value.dict_contents()
                        if _debug: YjApp._debug("    - value: %r", value)
                        apdudickey = str(propertyIdentifier)
                        apdudic[apdudickey] = value
                    sys.stdout.flush()
                for cmd_str in self.devname.keys():
                    if re.search(ati, self.devname[cmd_str]):
                        apdudic = {cmd_str:apdudic}
#                 print apdudic
                try:
                    ###########################################################################################################
#                     apdudic = init_set.Gzip_Strio.file2zipbuf(sys.path[0] + '/260002.xls',1)
#                     print apdudic
                    if ati in self.cmd_msg:
                        self.cmdackpub(ati, apdudic)
                    else:
                        if  (apdudic.keys()[0] not in self.apdudic \
                                or apdudic.values()[0] != self.apdudic[apdudic.keys()[0]]) \
                                or int(self.eviron["awaysupdate"]):
                            self.apdudic.update(apdudic)
                            pdata = self.make_pub_update_msg(apdudic)
                            self._info(json.dumps(pdata, indent=1))
                            self.publish(json.dumps(pdata, ensure_ascii=False, indent=1, encoding='utf-8'))
                            for ws in self.users:
                                ws.send(json.dumps(pdata, ensure_ascii=False, indent=1, encoding='utf-8'))
                except:
                    self._error(sys.exc_info()[1])

    def indication(self, apdu):
#         print json.dumps(apdu.dict_contents(), indent=1)
        if (apdu.dict_contents()["source"], apdu.dict_contents()["iAmDeviceIdentifier"]) not in self.whoislist:
            self._debug("update whoislist")
            self.whoislist[(apdu.dict_contents()["source"], apdu.dict_contents()["iAmDeviceIdentifier"])] = None
        if (isinstance(self._request, WhoIsRequest)) and (isinstance(apdu, IAmRequest)):
            device_type, device_instance = apdu.iAmDeviceIdentifier
            if device_type != 'device':
                raise DecodingError, "invalid object type"

            if (self._request.deviceInstanceRangeLowLimit is not None) and \
                (device_instance < self._request.deviceInstanceRangeLowLimit):
                pass
            elif (self._request.deviceInstanceRangeHighLimit is not None) and \
                (device_instance > self._request.deviceInstanceRangeHighLimit):
                pass
            else:
                self._debug('pduSource=%r',apdu.pduSource)
                self._debug('iAmDeviceIdentifier=%r',apdu.iAmDeviceIdentifier)
                self._debug('segmentationSupported=%r',apdu.segmentationSupported)
                self._debug('vendorID=%r',apdu.vendorID)
                whois = {\
                         'pduSource':repr(apdu.pduSource)\
                          , 'iAmDeviceIdentifier':str(apdu.iAmDeviceIdentifier)\
                          , 'segmentationSupported':str(apdu.segmentationSupported)\
                          , 'vendorID':str(apdu.vendorID)\
                          }
                try:
                    pdata = self.make_pub_update_msg(whois)
                    self.publish(json.dumps(pdata, ensure_ascii=False, indent=1, encoding='gbk'))
                except:
                    print sys.exc_info()[1]
        # forward it along
#         BIPSimpleApplication.indication(self, apdu)
if __name__ == '__main__':
    try:
        # parse the command line arguments
        args = ConfigArgumentParser(description=__doc__).parse_args()
        # make a device object
        this_device = LocalDeviceObject(
            objectName=args.ini.objectname,
            objectIdentifier=int(args.ini.objectidentifier),
            maxApduLengthAccepted=int(args.ini.maxapdulengthaccepted),
            segmentationSupported=args.ini.segmentationsupported,
            vendorIdentifier=int(args.ini.vendoridentifier),
            )

        # build a bit string that knows about the bit names
        pss = ServicesSupported()
        pss['whoIs'] = 1
        pss['iAm'] = 1
        pss['readProperty'] = 1
        pss['writeProperty'] = 1
        # set the property value to be just the bits
        this_device.protocolServicesSupported = pss.value
        yjiongApp = YjApp(this_device, yj_global.CONFIGPATH, __name__)

        @yjiongApp.route('/')
        def index():
            return render_template('client.html', lip=yjiongApp.eviron["external_ip"])

        @yjiongApp.route('/message')
        def message():
            if request.environ.get('wsgi.websocket'):
                ws = request.environ['wsgi.websocket']
                yjiongApp.users.add(ws)
                try:
                    while not ws.closed:
                        message = ws.receive()
                        if message:
                            try:
                                yjiongApp.base_message(wsmsg=message, wsobj=ws)
                            except WebSocketError:
                                print  sys.exc_info()
                                print "no user"
                    yjiongApp.users.remove(ws)
                    print 'Expected WebSocket request.'
                except:
                    yjiongApp._error(sys.exc_info()[1])
                    print  sys.exc_info()
            return "byebye"
        wsserver = WSGIServer(("0.0.0.0", 8000), yjiongApp, handler_class=WebSocketHandler)
    ############################################################################################################################
        def autoread():
            while True:
                if int(yjiongApp.eviron['_interval']) > 0:
                    if yjiongApp.devname:
                        for dev in yjiongApp.devname:
                            yjiongApp.doreadpropertymultiple(yjiongApp.devname[dev].encode("utf-8"))
                            time.sleep(1)
                    else:
                        if not yjiongApp.whoislist:
                            yjiongApp._debug("in get whoislist")
                            yjiongApp.get_whoislist()
                            time.sleep (5)
                        for addr_obidf, oblist in yjiongApp.whoislist.items():
                            yjiongApp._debug("whoislist.items: %r,%r",addr_obidf,oblist)
                            if not oblist:
                                yjiongApp._debug("get oblist")
                                yjiongApp.doreadproperty(addr_obidf[0], addr_obidf[1][0], (addr_obidf[1][1]), 'objectList')
                                time.sleep(1)
                                continue
                            else:
                                for obidf in oblist:
                                    yjiongApp._debug("read %r property",obidf)
                                    yjiongApp.doreadpropertymultiple((addr_obidf[0], obidf[0], obidf[1], 'all'))
                                    time.sleep(1)
                time.sleep(int(yjiongApp.eviron['_interval']))
#         myge = gevent.spawn(autoread)
#         myge.start
        gevent.joinall([gevent.spawn(run, (yjiongApp)),
                        gevent.spawn(autoread),
                        gevent.spawn(wsserver.serve_forever)
                        ])

#         request = ReadPropertyRequest(
#             objectIdentifier=('device', 260002),
#             propertyIdentifier='objectList',
#             )
#         request.pduDestination = Address('192.168.1.44')
#         deferred(yjiongApp.request, request)
#         deferred(yjiongApp.dowritepropert, '192.168.1.44 analogOutput 0 presentValue 3')
#         deferred(yjiongApp.get_whoislist)
#         deferred(yjiongApp.doreadpropertymultiple, ('192.168.1.44','analogInput',2,'all'))
#         deferred(yjiongApp.doreadpropertymultiple, yjiongApp.devname['xindong'].encode("utf-8"))
    ############################################################################################################################
#         run(yjiongApp)
    except Exception, e:
#         print type(sys.exc_info()[1][1])
        YjApp._error(sys.exc_info()[1])
        _log.exception("an error has occurred: %s", e)

